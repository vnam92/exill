import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Header.css';
import Link from '../Link';
import Navigation from '../Navigation';
import logoUrl from './logo-small.png';
import exillLogo from './exill_logo.png';
import logoUrl2x from './logo-small@2x.png';

class Header extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <Navigation />
          <Link className={s.brand} to="/">
            <img
              src={exillLogo}
              srcSet={`${logoUrl2x} 2x`}
              width="60"
              height="30"
              alt="React"
            />
            <span className={s.brandTxt}>Exill</span>
          </Link>
          <div className={s.banner}>
            <h1 className={s.bannerTitle}>EXILL</h1>
            <p className={s.bannerDesc}>Норммирование и оптимизация труда</p>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(s)(Header);
